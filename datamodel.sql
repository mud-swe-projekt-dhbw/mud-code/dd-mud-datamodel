CREATE TABLE user_data (
    id SERIAL PRIMARY KEY,
    username VARCHAR NOT NULL,
    password_hash VARCHAR NOT NULL,
    rank INTEGER NOT NULL
);

CREATE TABLE game (
    id SERIAL PRIMARY KEY,
    game_name VARCHAR NOT NULL,
    user_id INTEGER REFERENCES user_data(id)
);

CREATE TABLE player_game_member (
    user_id INTEGER REFERENCES user_data(id),
    game_id INTEGER REFERENCES game(id),
    PRIMARY KEY (user_id, game_id)
);

CREATE TABLE room (
    id SERIAL PRIMARY KEY,
    room_name VARCHAR NOT NULL,
    coordX INTEGER NOT NULL,
    coordY INTEGER NOT NULL,
    descrip VARCHAR,
    game_id INTEGER REFERENCES game(id)
);

CREATE TABLE gameaction (
    id SERIAL PRIMARY KEY,
    action_name VARCHAR NOT NULL,
    action_result VARCHAR NOT NULL,
    room_id INTEGER REFERENCES room(id) DEFAULT NULL --Bei null eine globale aktion
);

CREATE TABLE ability (
    id SERIAL PRIMARY KEY,
    ability_name VARCHAR NOT NULL
);

CREATE TABLE class (
    id SERIAL PRIMARY KEY,
    class_name VARCHAR NOT NULL,
    game_id INTEGER REFERENCES game(id)
);

CREATE TABLE class_ability (
    class_id INTEGER REFERENCES class(id),
    ability_id INTEGER REFERENCES ability(id),
    PRIMARY KEY (class_id, ability_id)
);

CREATE TABLE race (
    id SERIAL PRIMARY KEY,
    race_name VARCHAR NOT NULL,
    game_id INTEGER REFERENCES game(id)
);

CREATE TABLE race_ability (
    race_id INTEGER REFERENCES race(id),
    ability_id INTEGER REFERENCES ability(id),
    PRIMARY KEY (race_id, ability_id)
);

CREATE TABLE charakter (
    id SERIAL PRIMARY KEY,
    ch_name VARCHAR NOT NULL,
    ch_level INTEGER DEFAULT 1,
    hp INTEGER DEFAULT 1000,
    damage INTEGER DEFAULT 100,
    class_id INTEGER REFERENCES class(id),
    race_id INTEGER REFERENCES race(id),
    user_id INTEGER REFERENCES user_data(id),
    game_id INTEGER REFERENCES game(id),
    description VARCHAR,
    personality VARCHAR
);

CREATE TABLE item (
    id SERIAL PRIMARY KEY,
    item_type VARCHAR,
    item_name VARCHAR,
    item_value INTEGER,
    game_id INTEGER REFERENCES game(id)
);

CREATE TABLE item_charakter (
    ch_id INTEGER REFERENCES charakter(id),
    item_id INTEGER REFERENCES item(id),
    PRIMARY KEY (ch_id, item_id)
);

CREATE TABLE item_room (
    room_id INTEGER REFERENCES room(id),
    item_id INTEGER REFERENCES item(id),
    PRIMARY KEY (room_id, item_id)
);

CREATE TABLE charakter_room (
    charakter_id INTEGER REFERENCES charakter(id),
    room_id INTEGER REFERENCES room(id),
    PRIMARY KEY (charakter_id, room_id)
);